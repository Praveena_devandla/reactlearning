import logo from './logo.svg';
import './App.css';
// import Classcomponents from './Classcomponents';
// import Learning from './Components/Learning';
// import StateF from './Components/StateF';
// import StateC from './Components/StateC';
// import Table from './Components/Table';
// import Bootstrap from './Components/Bootstrap';
// import UseRef from './Components/UseRef';
// import UseEffect from './Components/UseEffect';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import Table from './Components/Table';
import ClassComponent from './Components/Classcomponents';
import Navbar from './Components/Navbar';
import Register from './Components/Register';
import Login from './Components/Login';
import Home from './Components/Home';
function App() {
  return (
    <div>
    {/* <h1>hello</h1>
    <h1>hello</h1>
   <Learning language = "react js"/>
    <Learning language = "angular js"/>
    <Learning language = "angular js"/>
    <StateF/>
    <StateC/> 
    <Table/>*/}
    {/* <Table/>
    <Bootstrap/> */}
    {/* <UseRef/> */}
    {/* <UseEffect/> */}
  {/* <Bootstrap/>  */}
  {/* <Classcomponents/> */}
  <BrowserRouter>
  <Navbar/>
  <switch>
    <Routes>
    <Route path='/Home' Component={Home}></Route>
      <Route path='/Register' Component={Register}></Route>
      <Route path='/Login' Component={Login}></Route>
      <Route path='/Classcomponents' Component={ClassComponent}></Route>
      <Route path='/Table' Component={Table}></Route>


    </Routes>
  </switch>
  </BrowserRouter>

    </div>
  );
}

export default App;
