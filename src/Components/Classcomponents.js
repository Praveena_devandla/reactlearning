import React, { Component } from 'react'

export class ClassComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            cards: [
                { image: 'download.jpeg', title: 'Moon', discription: 'Moon comes at night' },
                { image: 'nature1.jpg', title: 'Water Fall', discription: 'WaterFall with water' },
                { image: 'photo.jpeg', title: 'Sun Set', discription: 'Sun sets in the west' }
            ]
        }
    }
    render() {
        return (
            <div>

                {/* <div class='container'>
                    <div class='row'>
                        <div class='col-lg-4'>
                            {this.state.cards.map((cards, index) => (
                                <div class="card">
                                    <img src={cards.image} class="card-img-top" alt="..."></img>
                                    <div class="card-body">
                                        <h5 class="card-title">{cards.title}</h5>
                                        <p class="card-text">{cards.discription}</p>
                                        <a href="#" class="btn btn-primary">Button</a>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>

                </div> */}
               
                    <div class='container'>
                        <div class='row'>
                        {this.state.cards.map((cards, index) => (
                            <div class='col-lg-4'>
                                <div class="card">
                                    <img src={cards.image} class="card-img-top" alt="..."></img>
                                    <div class="card-body">
                                        <h5 class="card-title">{cards.title}</h5>
                                        <p class="card-text">{cards.discription}</p>
                                        <a href="#" class="btn btn-primary">Button</a>
                                    </div>
                                </div>
                            </div>
                                ))}
                        </div>

                    </div>
            


            </div>
        )
    }
}

export default ClassComponent