import React, { useState } from 'react'
import data from "./Data.json"
function Table() {
    const [contacts ,setcontacts] = useState(data);
  return (
    <div className='app'>
    <table>
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Mail id</th>
          <th>Mobile No</th>
        </tr>
      </thead>
      <tbody>
        {contacts.map((contact) =>(
            <tr>
            <td>{contact.FirstName}</td>
            <td>{contact.LastName}</td>
            <td>{contact.Mailid}</td>
            <td>{contact.phone}</td>
          </tr>
        ) )}
      </tbody>
    </table>
    </div>
  )
}
export default Table