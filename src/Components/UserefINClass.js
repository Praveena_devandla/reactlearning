import React, { Component } from 'react'

 class UserefINClass extends Component {
  constructor(props) {
    super(props)

    this.name=React.createRef();
    
}
componentDidMount(){
    this.name.current.focus();
}
render(){
    return (
      <div>
        <input type="text" ref={this.name}/>
      </div>
    )
  }
}

export default UserefINClass