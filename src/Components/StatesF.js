import React,{useState} from 'react'

function StatesF() {
    const [name,setName] = useState("Operation");
    const [count,setCount] = useState(0);
    const handleIncement =()=>{
        setName("Incrementing")
        setCount(count+1);
    }
    const handleDecrement=()=>{
        setName("Decrementing")
        if (count<1){
            return ""
        }
        else{
            setCount(count-1)
        }

    }
  return (
    <div>
        <h1>{name}</h1>
        <h1>{count}</h1>
        <button onClick={handleIncement}>Increment</button>
        <button onClick={handleDecrement}>Decrement</button>
    </div>
  )
}

export default StatesF