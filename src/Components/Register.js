
import React, { useState } from 'react';

const Register = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const handleRegister = (e) => {
    e.preventDefault();
    // Add registration logic here (e.g., send request to server)
    console.log(`Username: ${username}, Password: ${password}`);
  };

  return (
    <div className='registration-container'>
      <div>
    <form onSubmit={handleRegister}>
      <div className='div'>
        <label>
          Username:
          <input
            type="text"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </label>
      </div>
      <div className='div'>
        <label>
          Password:
          <input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </label>
      </div>
      <div className='div'>
        <label>
          Confirm Password:
          <input
            type="password"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
        </label>
      </div>
      <button className='btn1' type="submit">Register</button>
    </form>
  </div>
  </div>

  );
};

export default Register;